﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace IPrange
{
    class Program
    {
        static void Main(string[] args)
        {
            IPAddress start_ip;
            IPAddress end_ip;

            try
            {
                IPAddress.TryParse(args[0], out start_ip);
                IPAddress.TryParse(args[1], out end_ip);
            }
            catch
            {
                Console.WriteLine("Bad input params");
                return;
            }

            if (start_ip.AddressFamily != start_ip.AddressFamily)
            {
                Console.WriteLine("Different address types");
                return;
            }

            var start_ip_ba = start_ip.GetAddressBytes();
            var end_ip_ba = end_ip.GetAddressBytes();

            if (Compare(start_ip_ba, end_ip_ba) > -1)
            {
                Console.WriteLine("Bad range");
                return;
            }

            while (Compare(start_ip_ba, end_ip_ba) == -1)
            {
                for (int i = start_ip_ba.Length - 1; i >= 0; i--)
                {
                    if (++start_ip_ba[i] != 0)
                    {
                        IPAddress ip = new IPAddress(start_ip_ba);
                        Console.WriteLine(ip);
                        break;
                    }
                }
            }

            Console.ReadKey();
        }

        // 1 if first is greater
        // 0 if first and second are equal
        // -1 if first is less
        static int Compare(byte[] first, byte[] second)
        {
            for (int i = 0; i < first.Length; i++)
                if (first[i] > second[i])
                    return 1;
                else if (first[i] < second[i])
                    return -1;
                else
                    continue;

            return 0;
        }
    }
}
